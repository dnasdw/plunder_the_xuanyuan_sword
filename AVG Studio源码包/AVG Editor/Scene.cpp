// Scene.cpp: implementation of the CScene class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Editor.h"
#include "Scene.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
//
CScene::CScene()
{	//防止销毁的时候出错
	m_pAct = new ACT[1];
	m_pClu = new CLU[1];
};
//
CScene::~CScene()
{
	delete [] m_pAct;
	delete [] m_pClu;
};
//
BOOL CScene::New()
{
	m_nID = 1;
	m_nType = 1;
	m_nActNum = 0;
	m_nCurAct = 0;
	delete [] m_pAct;
	m_pAct = new ACT[1];
	m_nCluNum = 0;
	m_nCurClu = 0;
	delete [] m_pClu;
	m_pClu = new CLU[1];
	return TRUE;
}
//
BOOL CScene::Open(char* FileName)
{
	HANDLE hFile;
	hFile = CreateFile(FileName,GENERIC_READ,FILE_SHARE_READ,0,OPEN_EXISTING,0,NULL);
	if(hFile==INVALID_HANDLE_VALUE)
	{
		return FALSE;
	}
	DWORD dwActualSize = 0;
	BOOL bReadSt;
	//读出游戏进程ID
	bReadSt = ReadFile(hFile,&m_nID,sizeof(int),&dwActualSize,NULL);
	if( !bReadSt )
	{
		CloseHandle(hFile);
		return FALSE;
	}
	//读出场类型
	bReadSt = ReadFile(hFile,&m_nType,sizeof(int),&dwActualSize,NULL);
	if( !bReadSt )
	{
		CloseHandle(hFile);
		return FALSE;
	}
	//读出动作指令的数目
	bReadSt = ReadFile(hFile,&m_nActNum,sizeof(int),&dwActualSize,NULL);
	if( !bReadSt )
	{
		CloseHandle(hFile);
		return FALSE;
	}
	//
	if( m_nActNum != 0 )
	{
		//根据动作指令的数目创建动作指令数组
		delete [] m_pAct;
		m_pAct = new ACT[m_nActNum];
		//读出动作指令数组
		bReadSt = ReadFile(hFile,m_pAct,(sizeof(ACT)*m_nActNum),&dwActualSize,NULL);
		if( !bReadSt )
		{
			CloseHandle(hFile);
			return FALSE;
		}
	}
	//读出计算指令的数目
	bReadSt = ReadFile(hFile,&m_nCluNum,sizeof(int),&dwActualSize,NULL);
	if( !bReadSt )
	{
		CloseHandle(hFile);
		return FALSE;
	}
	//
	if( m_nCluNum != 0 )
	{
		//根据计算指令的数目创建动作指令数组
		delete [] m_pClu;
		m_pClu = new CLU[m_nCluNum];
		//读出计算指令数组
		bReadSt = ReadFile(hFile,m_pClu,(sizeof(CLU)*m_nCluNum),&dwActualSize,NULL);
		if( !bReadSt )
		{
			CloseHandle(hFile);
			return FALSE;
		}
	}
	//
	CloseHandle(hFile);
	//执行初始化的操作
	m_nCurAct = 0;
	m_nCurClu = 0;
	return TRUE;
};
//
BOOL CScene::Save(char* FileName)
{
	//写入文件
	HANDLE hFile;
	hFile = CreateFile(FileName,GENERIC_READ|GENERIC_WRITE,FILE_SHARE_READ,0,CREATE_ALWAYS,0,NULL);
	if(hFile==INVALID_HANDLE_VALUE)
	{
		return FALSE;
	}
	DWORD dwWritten = 0;
	BOOL bWriteSt;
	//写入游戏进程ID
	bWriteSt = WriteFile(hFile,&m_nID,sizeof(int),&dwWritten,NULL);
	if ( !bWriteSt )
	{
		CloseHandle(hFile);
		return FALSE;
	}
	//写入场类型
	bWriteSt = WriteFile(hFile,&m_nType,sizeof(int),&dwWritten,NULL);
	if ( !bWriteSt )
	{
		CloseHandle(hFile);
		return FALSE;
	}
	//写入动作指令的数目
	bWriteSt = WriteFile(hFile,&m_nActNum,sizeof(int),&dwWritten,NULL);
	if ( !bWriteSt )
	{
		CloseHandle(hFile);
		return FALSE;
	}
	//写入动作指令数组
	if( m_nActNum != 0 )
	{
		bWriteSt = WriteFile(hFile,m_pAct,(sizeof(ACT)*m_nActNum),&dwWritten,NULL);
		if ( !bWriteSt )
		{
			CloseHandle(hFile);
			return FALSE;
		}
	}
	//写入计算指令的数目
	bWriteSt = WriteFile(hFile,&m_nCluNum,sizeof(int),&dwWritten,NULL);
	if ( !bWriteSt )
	{
		CloseHandle(hFile);
		return FALSE;
	}
	//写入计算指令数组
	if( m_nCluNum != 0 )
	{
		bWriteSt = WriteFile(hFile,m_pClu,(sizeof(CLU)*m_nCluNum),&dwWritten,NULL);
		if ( !bWriteSt )
		{
			CloseHandle(hFile);
			return FALSE;
		}
	}
	//
	CloseHandle(hFile);
	return TRUE;
};
//
int CScene::SetID(int nID)
{
	int n = m_nID;
	m_nID = nID;
	return n;
}
//
int CScene::SetType(int nType)
{
	int n = m_nType;
	m_nType = nType;
	return n;
}
//
int CScene::GetID()
{
	return m_nID;
}
//
int CScene::GetType()
{
	return m_nType;
}
//
int CScene::GetActNum()
{
	return m_nActNum;
}
//
int CScene::GetCluNum()
{
	return m_nCluNum;
}
//添加一个动作指令
BOOL CScene::AddAct(ACT* pAct)
{
	//数组长度加一
	m_nActNum ++;
	//生成一个比当前数组大一的临时数组
	BYTE* ptemp = new BYTE[ m_nActNum*sizeof(ACT) ];
	//清空内存
	memset( ptemp, 0x0, m_nActNum*sizeof(ACT) );
	//将老数组拷贝入临时数组
	memcpy( ptemp, m_pAct, m_nActNum*sizeof(ACT) );
	//将传入的参数添加在临时数组的末尾
	memcpy( &ptemp[(m_nActNum-1)*sizeof(ACT)], pAct, sizeof(ACT) );
	//删除老数组
	delete [] m_pAct;
	//将临时数组的指针传递给老数组
	m_pAct = (ACT*) ptemp;
	//
	return TRUE;
}
//插入一个动作指令（在nIndex的前面插入一个新的事件）
BOOL CScene::InsertAct(int nIndex,ACT* pAct)
{
	//数组长度加一
	m_nActNum ++;
	//生成一个比当前数组大一的临时数组
	BYTE* ptemp = new BYTE[ m_nActNum*sizeof(ACT) ];
	//清空内存
	memset( ptemp, 0x0, m_nActNum*sizeof(ACT) );
	//将插入位置前的数据拷贝入新数组
	memcpy( ptemp, m_pAct, nIndex*sizeof(ACT) );
	//将传入的参数添加在后面
	memcpy( &ptemp[nIndex*sizeof(ACT)], pAct, sizeof(ACT) );
	//将老数组剩下的部分添加在后面
	memcpy( &ptemp[(nIndex+1)*sizeof(ACT)], &m_pAct[nIndex], (m_nActNum-nIndex-1)*sizeof(ACT) );
	//删除老数组
	delete [] m_pAct;
	//将临时数组的指针传递给老数组
	m_pAct = (ACT*) ptemp;
	//
	return TRUE;
}
//修改一个动作指令
BOOL CScene::SetAct(int nIndex,ACT* pAct)
{
	if( nIndex > m_nActNum-1 )
		return FALSE;
	//
	memcpy( &m_pAct[nIndex], pAct, sizeof(ACT) );
	return TRUE;
}
//删除一个动作指令
BOOL CScene::DelAct(int nIndex)
{
	//数组长度减一
	m_nActNum --;
	//生成一个比当前数组小一的临时数组
	BYTE* ptemp = new BYTE[ m_nActNum*sizeof(ACT) ];
	//清空内存
	memset( ptemp, 0x0, m_nActNum*sizeof(ACT) );
	//将删除位置前的数据拷贝入新数组
	memcpy( ptemp, m_pAct, nIndex*sizeof(ACT) );
	//将删除位置后的部分添加在后面
	memcpy( &ptemp[nIndex*sizeof(ACT)], &m_pAct[nIndex+1], (m_nActNum-nIndex)*sizeof(ACT) );
	//删除老数组
	delete [] m_pAct;
	//将临时数组的指针传递给老数组
	m_pAct = (ACT*) ptemp;
	//
	return TRUE;
}
//读取一个动作指令
BOOL CScene::GetAct(int nIndex,ACT* pAct)
{
	if( nIndex > m_nActNum-1 )
		return FALSE;
	//
	memcpy( pAct, &m_pAct[nIndex], sizeof(ACT) );
	return TRUE;
}
//
//添加一个计算指令
BOOL CScene::AddClu(CLU* pClu)
{
	//数组长度加一
	m_nCluNum ++;
	//生成一个比当前数组大一的临时数组
	BYTE* ptemp = new BYTE[ m_nCluNum*sizeof(CLU) ];
	//清空内存
	memset( ptemp, 0x0, m_nCluNum*sizeof(CLU) );
	//将老数组拷贝入临时数组
	memcpy( ptemp, m_pClu, m_nCluNum*sizeof(CLU) );
	//将传入的参数添加在临时数组的末尾
	memcpy( &ptemp[(m_nCluNum-1)*sizeof(CLU)], pClu, sizeof(CLU) );
	//删除老数组
	delete [] m_pClu;
	//将临时数组的指针传递给老数组
	m_pClu = (CLU*) ptemp;
	//
	return TRUE;
}
//插入一个计算指令（在nIndex的前面插入一个新的事件）
BOOL CScene::InsertClu(int nIndex,CLU* pClu)
{
	//数组长度加一
	m_nCluNum ++;
	//生成一个比当前数组大一的临时数组
	BYTE* ptemp = new BYTE[ m_nCluNum*sizeof(CLU) ];
	//清空内存
	memset( ptemp, 0x0, m_nCluNum*sizeof(CLU) );
	//将插入位置前的数据拷贝入新数组
	memcpy( ptemp, m_pClu, nIndex*sizeof(CLU) );
	//将传入的参数添加在后面
	memcpy( &ptemp[nIndex*sizeof(CLU)], pClu, sizeof(CLU) );
	//将老数组剩下的部分添加在后面
	memcpy( &ptemp[(nIndex+1)*sizeof(CLU)], &m_pClu[nIndex], (m_nCluNum-nIndex-1)*sizeof(CLU) );
	//删除老数组
	delete [] m_pClu;
	//将临时数组的指针传递给老数组
	m_pClu = (CLU*) ptemp;
	//
	return TRUE;
}
//修改一个计算指令
BOOL CScene::SetClu(int nIndex,CLU* pClu)
{
	if( nIndex > m_nCluNum-1 )
		return FALSE;
	//
	memcpy( &m_pClu[nIndex], pClu, sizeof(CLU) );
	return TRUE;
}
//删除一个计算指令
BOOL CScene::DelClu(int nIndex)
{
	//数组长度减一
	m_nCluNum --;
	//生成一个比当前数组小一的临时数组
	BYTE* ptemp = new BYTE[ m_nCluNum*sizeof(CLU) ];
	//清空内存
	memset( ptemp, 0x0, m_nCluNum*sizeof(CLU) );
	//将删除位置前的数据拷贝入新数组
	memcpy( ptemp, m_pClu, nIndex*sizeof(CLU) );
	//将删除位置后的部分添加在后面
	memcpy( &ptemp[nIndex*sizeof(CLU)], &m_pClu[nIndex+1], (m_nCluNum-nIndex)*sizeof(CLU) );
	//删除老数组
	delete [] m_pClu;
	//将临时数组的指针传递给老数组
	m_pClu = (CLU*) ptemp;
	//
	return TRUE;
}
//读取一个计算指令
BOOL CScene::GetClu(int nIndex,CLU* pClu)
{
	if( nIndex > m_nCluNum-1 )
		return FALSE;
	//
	memcpy( pClu, &m_pClu[nIndex], sizeof(CLU) );
	return TRUE;
}
//