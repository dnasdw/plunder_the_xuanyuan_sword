// Avi.cpp

#include "stdafx.h"
#include "Game.h"

CAvi::CAvi()
{
	m_bShow = FALSE;
	m_lLastChangeTime = 0;
	m_x = 0;
	m_y = 0;
	m_w = 0;
	m_h = 0;
	m_nTileX = 0;
	m_nTileY = 0;
	m_nTileW = 0;
	m_nTileH = 0;
	m_nArrayMode = 0;
	m_nTileCount = 0;
	m_nCurTile = 0;
	m_nTimes = 0;
	m_nInterval = 0;
	m_nOverMode = 0;
	m_nOffsetX = 0;
	m_nOffsetY = 0;
	memset( m_psFileName, 0x0, 256 );
}

CAvi::~CAvi()
{
}

// 初始化，打开资源文件，构造必要的DC
BOOL CAvi::Init( CDC* pDC )
{
	m_w = 0;
	m_h = 0;
	m_MemDC.CreateCompatibleDC( pDC );
	CBitmap* pBmp = new CBitmap;
	pBmp->CreateCompatibleBitmap( pDC, m_w, m_h );
	DeleteObject( m_MemDC.SelectObject( pBmp ) );
	return TRUE;
}

// 打开文件，设置诸项参数
// 单元图片要么横向排列，要么纵向排列。
// 横向排列时，nArrayMode参数设置为1，nSpace是单元图片的宽度
// 纵向排列时，nArrayMode参数设置为2，nSpace是单元图片的高度
BOOL CAvi::Open( char* psFileName,	// 文件名
		   int x,               // 初始显示位置X
		   int y,               // 初始显示位置Y
		   int nTileX,          // 动画起始单元的X
		   int nTileY,          // 动画起始单元的Y
		   int nTileW,          // 单元图片的宽度
		   int nTileH,          // 单元图片的高度
		   int nArrayMode,      // 排列方式：1:单元图片横向排列；2:单元图片纵向排列
		   int nTileCount,      // 动画循环总祯数
		   int nCurTile,        // 当前的动画祯
		   int nTimes,          // 动画循环的次数
		   int nInterval,       // 动画祯之间的间隔时间
		   int nOverMode,       // 动画显示完成后的处理：1:保持最后一祯；2:消失
		   int nOffsetX,        // 动画显示位置改变的偏移量X
		   int nOffsetY         // 动画显示位置改变的偏移量Y
		  )
{
	HBITMAP hbmp = (HBITMAP)LoadImage( NULL, psFileName, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE );
	if( hbmp == NULL )
		return FALSE;

	if( (nArrayMode < 1) || (nArrayMode > 2) )
		return FALSE;

	if( (nOverMode < 1) || (nOverMode > 2) )
		return FALSE;

	// 保存文件名
	memset( m_psFileName, 0x0, 256 );
	strncpy( m_psFileName, psFileName, 255 );

	DeleteObject( m_MemDC.SelectObject(hbmp) );

	// 取得图片的宽高
	CBitmap* pBmp = m_MemDC.GetCurrentBitmap();
	BITMAP stBitmap; 
	pBmp->GetObject( sizeof(BITMAP), &stBitmap );
	m_w = stBitmap.bmWidth;
	m_h = stBitmap.bmHeight;

	m_x = x;
	m_y = y;

	m_nTileW = nTileW;
	m_nTileH = nTileH;

	// 单元图片在整张图片中的定位
	// 单元图片的宽度和高度
	if( nArrayMode == 1 )
	{
		// 单元图片横向排列
		m_nTileX = nTileX + nCurTile * nTileW;
		m_nTileY = nTileY;

	}
	else
	{
		// 单元图片纵向排列
		m_nTileX = nTileX;
		m_nTileY = nTileY + nCurTile * nTileH;
	}

	// 单元图片排列方式：1:单元图片横向排列；2:单元图片纵向排列
	m_nArrayMode = nArrayMode;
	// 一个动画循环的总祯数
	m_nTileCount = nTileCount;
	// 当前动画的祯
	m_nCurTile = nCurTile;
	// 动画循环的次数（-1表示无限循环）
	m_nTimes = nTimes;
	// 每祯动画间隔的时间
	m_nInterval = nInterval;
	// 动画显示完成后的处理：1:保持最后一祯；2:消失
	m_nOverMode = nOverMode;
	// 每一祯动画显示位置改变的偏移量
	m_nOffsetX = nOffsetX;
	m_nOffsetY = nOffsetY;

	m_lLastChangeTime = 0;

	// 允许显示
 	m_bShow = TRUE;
	return TRUE;
}

// 绘制
void CAvi::Draw( CDC* pDC, int x, int y )
{
	if( !m_bShow )
		return;

	HDC hdc = pDC->GetSafeHdc();
	HDC hMemDC = m_MemDC.GetSafeHdc();
	TransBlt( hdc,m_x+x,m_y+y,m_nTileW,m_nTileH,hMemDC,m_nTileX,m_nTileY,COLORKEY );
}

// 处理移动
BOOL CAvi::Proc( long lNow )
{
	if( !m_bShow )
		return FALSE;

	if( (lNow - m_lLastChangeTime) < m_nInterval )
		return TRUE;

	m_lLastChangeTime = lNow;

	// 换下一祯图片
	if( m_nCurTile == (m_nTileCount - 1) )
	{
		// 看是否完成了循环
		if( m_nTimes != -1 )
		{
			if( m_nTimes > 0 )
			{
				m_nTimes --;
			}
			else
			{
				// 完成了循环，决定保持静态画面还是消失
				if( m_nOverMode == 1 )
				{
					// 保持静态画面
					m_nTimes = -1;
					m_nTileCount = 1;
					m_nCurTile = 0;
					return TRUE;
				}
				else
				{
					m_bShow = FALSE;
				}
			}
		}

		// 最后一祯，将当前单元更换到第一祯位置
		m_nCurTile = 0;
		if( m_nArrayMode == 1 )
		{
			// 单元图片横向排列
			m_nTileX -= (m_nTileCount-1)*m_nTileW;
		}
		else
		{
			// 单元图片纵向排列
			m_nTileY -= (m_nTileCount-1)*m_nTileH;
		}

		if( (m_nTimes != -1) || (m_nTileCount != 1) || (m_nCurTile != 0) )
		{
			// 计算下一个显示位置
			m_x += m_nOffsetX;
			m_y += m_nOffsetY;
		}
	}
	else
	{
		// 将当前单元更换到下一祯的位置
		if( m_nTileCount > 1 )
		{
			m_nCurTile ++;
			if( m_nArrayMode == 1 )
			{
				// 单元图片横向排列
				m_nTileX += m_nTileW;
			}
			else
			{
				// 单元图片纵向排列
				m_nTileY += m_nTileH;
			}
		}

		// 计算下一个显示位置
		m_x += m_nOffsetX;
		m_y += m_nOffsetY;
	}

	return TRUE;
}

// 设置是否显示
BOOL CAvi::Show( BOOL bShow )
{
	BOOL bTemp = m_bShow;
	m_bShow = bShow;
	return bTemp;
}

// 将本类保存至字符串
BOOL CAvi::SaveToString( CString& strFile )
{
	strFile.Format( "%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%s;", 
		m_x, m_y, m_w, m_h, m_bShow, 
		m_nTileX, m_nTileY, m_nTileW, m_nTileH, m_nArrayMode, m_nTileCount,
		m_nCurTile, m_nTimes, m_nInterval, m_nOverMode, m_nOffsetX, m_nOffsetY,
		m_psFileName );
	return TRUE;
}

// 从字符串还原本类
BOOL CAvi::LoadFromString( CString& strFile )
{
	CString strTmp;
	int k = 0;
	if( !GetSlice( strTmp, strFile, k ) )
		return FALSE;
	m_x = atoi( strTmp );

	k ++;
	if( !GetSlice( strTmp, strFile, k ) )
		return FALSE;
	m_y = atoi( strTmp );

	k ++;
	if( !GetSlice( strTmp, strFile, k ) )
		return FALSE;
	m_w = atoi( strTmp );

	k ++;
	if( !GetSlice( strTmp, strFile, k ) )
		return FALSE;
	m_h = atoi( strTmp );

	k ++;
	if( !GetSlice( strTmp, strFile, k ) )
		return FALSE;
	m_bShow = atoi( strTmp );

	k ++;
	if( !GetSlice( strTmp, strFile, k ) )
		return FALSE;
	m_nTileX = atoi( strTmp );

	k ++;
	if( !GetSlice( strTmp, strFile, k ) )
		return FALSE;
	m_nTileY = atoi( strTmp );

	k ++;
	if( !GetSlice( strTmp, strFile, k ) )
		return FALSE;
	m_nTileW = atoi( strTmp );

	k ++;
	if( !GetSlice( strTmp, strFile, k ) )
		return FALSE;
	m_nTileH = atoi( strTmp );

	k ++;
	if( !GetSlice( strTmp, strFile, k ) )
		return FALSE;
	m_nArrayMode = atoi( strTmp );

	k ++;
	if( !GetSlice( strTmp, strFile, k ) )
		return FALSE;
	m_nTileCount = atoi( strTmp );

	k ++;
	if( !GetSlice( strTmp, strFile, k ) )
		return FALSE;
	m_nCurTile = atoi( strTmp );

	k ++;
	if( !GetSlice( strTmp, strFile, k ) )
		return FALSE;
	m_nTimes = atoi( strTmp );

	k ++;
	if( !GetSlice( strTmp, strFile, k ) )
		return FALSE;
	m_nInterval = atoi( strTmp );

	k ++;
	if( !GetSlice( strTmp, strFile, k ) )
		return FALSE;
	m_nOverMode = atoi( strTmp );

	k ++;
	if( !GetSlice( strTmp, strFile, k ) )
		return FALSE;
	m_nOffsetX = atoi( strTmp );

	k ++;
	if( !GetSlice( strTmp, strFile, k ) )
		return FALSE;
	m_nOffsetY = atoi( strTmp );

	k ++;
	if( !GetSlice( strTmp, strFile, k ) )
		return FALSE;
	memset( m_psFileName, 0x0, 256 );
	strncpy( m_psFileName, strTmp, 255 );

	if( strlen(m_psFileName) > 0 )
	{
		// 重新调入图片
		HBITMAP hbmp = (HBITMAP)LoadImage( NULL, m_psFileName, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE );
		if( hbmp == NULL )
			return FALSE;
		DeleteObject( m_MemDC.SelectObject(hbmp) );
	}

	return TRUE;
}
/* END */