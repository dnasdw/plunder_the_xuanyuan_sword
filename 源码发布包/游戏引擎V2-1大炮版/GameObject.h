// GameObject.h: interface for the CGameObject class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(__GAMEOBJECT_H__)
#define __GAMEOBJECT_H__

class CGameMap;

class CGameObject
{
public:
	// 地图设备的指针
	CGameMap* m_pGameMap;
	// 以像素为单位的坐标系统
	int m_x;
	int m_y;
	int m_w;
	int m_h;
	// 所属图片设备
	HDC m_hImageDC;
	// 析构时图片是否需要删除（根据初始化图片的方法而定）
	BOOL m_bDeleteImage;
	// 死亡动画序列
	HDC m_hDeadDC;
	// 动作开始的时间（用于实现例如更换弹夹的延时）
	long m_lStartTime;

public:
	CGameObject( CGameMap* pGameMap );
	virtual ~CGameObject();

	// 初始化图片设备
	BOOL InitImage( char* psFileName );

	// 设置位置（左上角点的位置）
	BOOL SetPos( int x, int y );

	// 绘制
	virtual void Draw( HDC hDC, int x, int y );

	// 获得所在格子的坐标。每个精灵只能占一个格子的位置
	// 得到以格子为单位的坐标系统（用于地图绘制和碰撞检测）
	virtual BOOL GetGride( int* pX, int* pY );

	// 判断自己是否可以被显示
	virtual BOOL CanBeShown();

	// 判断是否与另外一个物体相交
	virtual BOOL IsObjectCut( CGameObject* pObj );

    // 得到被攻击检测矩形
	virtual BOOL GetHitRect( int* px, int* py, int* pw, int* ph );

    // 得到地图占用矩形
    virtual BOOL GetPlaceRect( int* px, int* py, int* pw, int* ph );

	// 移动
	virtual void Move( long lNow );
};

#endif // !defined(__GAMEOBJECT_H__)
