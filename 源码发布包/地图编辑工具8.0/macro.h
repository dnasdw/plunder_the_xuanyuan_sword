// macro.h

#ifndef __MACRO_H__
#define __MACOR_H__

// 地图“瓷砖”的宽度和高度
#define TILE_W             (32)
#define TILE_H             (32)

// 地图编辑区能显示多少块“瓷砖”
#define EDIT_AREA_GRID_W   (21)
#define EDIT_AREA_GRID_H   (21)
#define EDIT_AREA_W        (TILE_W * EDIT_AREA_GRID_W)
#define EDIT_AREA_H        (TILE_H * EDIT_AREA_GRID_H)
#define EDIT_PAGE_STEP     (10)

// “瓷砖”选择区的逻辑宽度和逻辑高度（即展开后的尺寸，虽然本软件只能显示出1/4）
#define MAP_TILES_LOGIC_W  (40)
#define MAP_TILES_LOGIC_H  (20)
#define MAP_TILES_DISP_W   (10)
#define MAP_TILES_DISP_H   (MAP_TILES_LOGIC_H)
#define MAP_TILES_PAGE_COUNT  (MAP_TILES_LOGIC_W / MAP_TILES_DISP_W)
#define MAP_TILES_LEFT     (698)
#define MAP_TILES_TOP      (6)
#define MAP_TILES_W        (TILE_W * MAP_TILES_DISP_W)
#define MAP_TILES_H        (TILE_H * MAP_TILES_DISP_H)
#define MAP_TILES_NUM_MAX  (MAP_TILES_LOGIC_W * MAP_TILES_LOGIC_W)
#define MAP_TILES_ZERO     (0)//0表示空

// “瓷砖”上部5行是可以行走的，其余部分不可通行
#define TILE_PASSABLE_ROW_COUNT  (5)

// 地图的大小，单位是“瓷砖”，包括编辑区以外看不见的部分
#define MAP_GRID_W         (200)
#define MAP_GRID_H         (200)

// 滚动条的宽度
#define SCROLL_BAR_W       (20)

// 信息栏的Y位置。信息栏在编辑区下方
#define INFO_BAR_Y         (EDIT_AREA_H + SCROLL_BAR_W + 4)

// 地图与缩略图的比例
#define MAP_TO_THUMBNAIL_SCALE      (5)
#define THUMBNAIL_W                 (MAP_GRID_W * MAP_TO_THUMBNAIL_SCALE)
#define THUMBNAIL_H                 (MAP_GRID_H * MAP_TO_THUMBNAIL_SCALE)



#endif