// GameMap.cpp: implementation of the CGameMap class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Game.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGameMap::CGameMap( CMainFrame* pMF )
{
	m_pMainFrm = pMF;
	m_nEventDelay = 0;
	m_hImageDC = NULL;

#ifdef DEBUG
	m_hNpcNumberDC = NULL;
#endif

	m_nLeft = 0;
	m_nTop  = 0;
	m_nNpcCount = 0;
	m_paNpcArray   = NULL;

	// 获取DC
	HDC hdc = ::GetDC( theApp.m_pMainWnd->m_hWnd );

#ifdef DEBUG
	HBITMAP hNpcsBmp=(HBITMAP)LoadImage(NULL,"codemap.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
	m_hNpcNumberDC = ::CreateCompatibleDC( hdc );
	::DeleteObject( ::SelectObject( m_hNpcNumberDC, hNpcsBmp ) );
#endif

	// 首先打开NPC图片数组
	int i;
	for( i=0; i<7; i++ )
	{
		char psImgFileName[256];
		sprintf( psImgFileName, "%s%d%s", PREFIX_NPC_IMAGE, i+1, SUFFIX_NPC_IMAGE );
		HBITMAP hBmp = (HBITMAP)LoadImage(NULL,psImgFileName,IMAGE_BITMAP,0,0,LR_LOADFROMFILE);

		m_hNpcImageDC[i] = ::CreateCompatibleDC( hdc );
		::DeleteObject( ::SelectObject( m_hNpcImageDC[i], hBmp ) );
	}

	// 打开NPC死亡图像
	HBITMAP hBmp1 = (HBITMAP)LoadImage(NULL,FILENAME_NPC_DEAD,IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
	m_hNpcDeadDC = ::CreateCompatibleDC( hdc );
	::DeleteObject( ::SelectObject( m_hNpcDeadDC, hBmp1 ) );

	// 释放DC
	::ReleaseDC( theApp.m_pMainWnd->m_hWnd, hdc );

	// 创建角色
	// （角色需要NPC死亡图像，所以需要在打开该图像之后创建）
	m_pHero = new CHero( this );

	// 打开NPC动作指令文件
	HANDLE hFile = CreateFile(FILENAME_NPC_ACTION,GENERIC_READ,FILE_SHARE_READ,0,OPEN_EXISTING,0,NULL);
	if(hFile==INVALID_HANDLE_VALUE)
		return;

	int nBufferSize = GRIDE_H*GRIDE_W*2;
	char* temp = new char[nBufferSize + 20];

	DWORD dwActualSize = 0;
	BOOL bReadSt = ReadFile(hFile,temp,sizeof(char)*nBufferSize,&dwActualSize,NULL);
		CloseHandle( hFile );
	if( !bReadSt )
	{
		delete [] temp;
		ReleaseMap();
		return;
	}

	m_psNpcAction = new char*[6];
	for( i=0; i<6; i++ )
	{
		m_psNpcAction[i] = new char[256];
		if( !GetLine(temp, m_psNpcAction[i], i) )
		{
			delete [] m_psNpcAction[i];
			m_psNpcAction[i] = NULL;
			delete [] temp;
			return;
		}
	}

	delete [] temp;

	m_pBoss = NULL;
}

// 从字符串中取得一行
BOOL CGameMap::GetLine( char* cString, char* cReturn, int k )
{
	if( cString == NULL )
	  return FALSE;
	
	int iStartPos  = 0;
	int iNextPos   = -1;
	int iStrLength = strlen (cString);

	int i;
	for (i=0; i<=k; i++)
	{
		// 把上次查找的位置给开始位置
		iStartPos += (iNextPos + 1);

		// 查找分隔符'\n'
		if (iStartPos < iStrLength)
		{
			iNextPos = strcspn ((cString+iStartPos), "\n");
		}
		else
		{
			return FALSE;
		}
	}
 
	// 将iStartPos和iEndPos之间的内容拷贝给temp
	char* cTemp = new char [iNextPos + 1];
	memset (cTemp, 0x0, iNextPos+1);
	memcpy (cTemp ,(cString+iStartPos), iNextPos);

	// 如果最末字符是“\r”，则去掉
	iStrLength = iNextPos;
	if (cTemp[iStrLength - 1] == '\r')
	{
		iStrLength --;
	}

	strncpy (cReturn, cTemp, iStrLength);
	cReturn[ iStrLength ] = '\0';

	delete [] cTemp;
	return TRUE;
}

CGameMap::~CGameMap()
{
	// 释放地图资源
	ReleaseMap();

	// 释放角色
	if( m_pHero != NULL )
	{
		delete m_pHero;
		m_pHero = NULL;
	}

	// 释放NPC图元
	int i;
	for( i=0; i<7; i++ )
	{
		if( m_hNpcImageDC[i] != NULL )
		{
			::DeleteDC( m_hNpcImageDC[i] );
			m_hNpcImageDC[i] = NULL;
		}
	}

	// 释放NPC动作数组
	if( m_psNpcAction != NULL )
	{
		for( i=0; i<6; i++ )
		{
			if( m_psNpcAction[i] != NULL )
				delete [] m_psNpcAction[i];
		}
		delete [] m_psNpcAction;
	}

	// 释放BOSS
	if( m_pBoss != NULL )
	{
		delete m_pBoss;
		m_pBoss = NULL;
	}
}

// 初始化地图
BOOL CGameMap::LoadMap( char* psImgFileName, char* psMapFileName, char* psNpcFileName )
{
	int k; int i; int j;

	// 如果本地图已经初始化则释放相关资源
	ReleaseMap();

	// 复位角色的状态
	int nLife  = 1000;
	int nLevel = 50;
	int nExp   = 0;

	if( m_pHero != NULL )
	{
		nLife  = m_pHero->m_nLife;
		nLevel = m_pHero->m_nLevel;
		nExp   = m_pHero->m_nExp;
		delete m_pHero;
	}

	m_pHero = new CHero( this );
	m_pHero->m_nLife  = nLife;
	m_pHero->m_nLevel = nLevel;
	m_pHero->m_nExp   = nExp;

	// 打开瓷砖位图
	HBITMAP hBmp=(HBITMAP)LoadImage(NULL,psImgFileName,IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
	if( hBmp == NULL )
		return FALSE;

	HDC hdc = ::GetDC( theApp.m_pMainWnd->m_hWnd );
	m_hImageDC = ::CreateCompatibleDC( hdc );
	::DeleteObject( ::SelectObject( m_hImageDC, hBmp ) );

	// 打开.map文件
	HANDLE hFile = CreateFile(psMapFileName,GENERIC_READ,FILE_SHARE_READ,0,OPEN_EXISTING,0,NULL);
	if(hFile==INVALID_HANDLE_VALUE)
		return FALSE;

	int nBufferSize = GRIDE_H*GRIDE_W*2;
	char* temp = new char[nBufferSize + 20];

	DWORD dwActualSize = 0;
	BOOL bReadSt = ReadFile(hFile,temp,sizeof(char)*nBufferSize,&dwActualSize,NULL);
		CloseHandle( hFile );
	if( !bReadSt )
	{
		::DeleteObject( hBmp );
		::ReleaseDC( theApp.m_pMainWnd->m_hWnd, hdc );
		delete [] temp;
		ReleaseMap();
		return FALSE;
	}

	for( i=0; i<GRIDE_H; i++ )
	{
		for( j=0; j<GRIDE_W; j++ )
		{
			k = ( (i*GRIDE_W+j) * 2 );
			map[i][j] = ( (temp[k]*100) + temp[k+1] );
		}
	}

	// 打开.npc文件
	if( psNpcFileName == NULL )
	{
		for( i=0; i<GRIDE_H; i++ )
		{
			for( j=0; j<GRIDE_W; j++ )
			{
				npc[i][j] = 0;
			}
		}
	}
	else
	{
		HANDLE hFile = CreateFile(psNpcFileName,GENERIC_READ,FILE_SHARE_READ,0,OPEN_EXISTING,0,NULL);
		if(hFile==INVALID_HANDLE_VALUE)
			return FALSE;

		DWORD dwActualSize = 0;
		BOOL bReadSt = ReadFile(hFile,temp,sizeof(char)*nBufferSize,&dwActualSize,NULL);
			CloseHandle(hFile);
		if( !bReadSt )
		{
			::DeleteObject( hBmp );
			::ReleaseDC( theApp.m_pMainWnd->m_hWnd, hdc );
			delete [] temp;
			ReleaseMap();
			return FALSE;
		}

		for( i=0; i<GRIDE_H; i++ )
		{
			for( j=0; j<GRIDE_W; j++ )
			{
				k = ( (i*GRIDE_W+j) * 2 );
				npc[i][j] = ( (temp[k]*100) + temp[k+1] );
			}
		}
	}

	delete [] temp;

	// 根据NPC矩阵初始化NPC阵列
	// 1>第一次扫描NPC矩阵确定NPC的数量并创建NPC数组
	m_nNpcCount = 0;
	int nType;
	int sx;
	int sy;
	for( sx=0; sx<200; sx++ )
	{
		for( sy=0; sy<200; sy++ )
		{
			nType = npc[sx][sy];
			if( (nType > 0) && (nType < 11) )
				m_nNpcCount ++;
		}
	}
	// 2>第二次扫描NPC矩阵并初始化NPC数组
	m_paNpcArray = new CNpc*[ m_nNpcCount ];
	i = 0;
	for( sx=0; sx<200; sx++ )
	{
		for( sy=0; sy<200; sy++ )
		{
			int nType = npc[sx][sy];
			if( (nType > 0) && (nType < 11) )
			{
				m_paNpcArray[i ++] = new CNpc( this, nType, sx*TILE_W, sy*TILE_H );
				npc[sx][sy] = 0;      // 在NPC矩阵中清除该npc信息
			}
		}
	}

	::ReleaseDC( theApp.m_pMainWnd->m_hWnd, hdc );
	return TRUE;
}

// 释放地图资源
BOOL CGameMap::ReleaseMap()
{
	if( m_hImageDC != NULL )
	{
		::DeleteDC( m_hImageDC );
		m_hImageDC = NULL;
	}

	// 释放NPC阵列
	CleanNpc();

	return TRUE;
}

// 碰撞检测（监测某物体与其他物体之间的碰撞情况）
CGameObject* CGameMap::HitTest( CGameObject* pObj )
{
	// 按照碰撞矩形进行检测
	if( m_pHero != NULL )
	{
		if( pObj != m_pHero )
		{
			if( m_pHero->IsObjectCut( pObj ) )
				return m_pHero;
		}
	}

	int i;
	for( i=0; i<m_nNpcCount; i++ )
	{
		if( pObj != m_paNpcArray[i] )
		{
			if( m_paNpcArray[i]->IsObjectCut( pObj ) )
				return m_paNpcArray[i];
		}
	}

	return NULL;
}

// 碰壁检测
BOOL CGameMap::HitWallTest( CGameObject* pObj )
{
	// 改进的地图检测方法
	// 需要检测四角是否都在可行走的范围内
	int x, y, w, h;
	pObj->GetHitTestRect(&x, &y, &w, &h);

	//往中心区域缩小几个像素
	const int margin = 8;
	x += margin;
	y += margin;
	w -= margin*2;
	h -= margin*2;

	// 按照格子进行检测
	int X = x / TILE_W;
	int Y = y / TILE_H;
	if (map[X][Y] > 200) return TRUE;

	X = (x + w) / TILE_W;
	if (map[X][Y] > 200) return TRUE;

	Y = (y + h) / TILE_H;
	if (map[X][Y] > 200) return TRUE;

	Y = y / TILE_H;
	if (map[X][Y] > 200) return TRUE;

	return FALSE;
}

// 事件检测
BOOL CGameMap::EventTest(  CGameObject* pObj )
{
	// 按照格子进行检测
//	int X, Y;
//	if( !(pObj->GetGride( &X, &Y )) )
//		return FALSE;

	// 改进的事件检测方法
	int x, y, w, h;
	if( !pObj->GetHitTestRect( &x, &y, &w, &h ) )
		return FALSE;

	int nEvent = npc[x / TILE_W][y / TILE_H];
	if( nEvent <= 0 )
		nEvent = npc[(x + w) / TILE_W][y / TILE_H];
	if( nEvent <= 0 )
		nEvent = npc[x / TILE_W][(y + h) / TILE_H];
	if( nEvent <= 0)
		nEvent = npc[(x + w) / TILE_W][(y + h) / TILE_H];

	if( nEvent > 6 )
	{
		if( m_nEventDelay == 0 )
		{
			// 防止事件被连续触发
			m_nEventDelay = EVENT_DELAY;
			// 调用事件处理函数
			m_pMainFrm->PostMessage( ID_GAME_MESSAGE, nEvent, 0 );
		}
		return TRUE;
	}

	return FALSE;
}

// 设置地图中心点
BOOL CGameMap::SetLeftTop( int x, int y )
{
	m_nLeft = x;
	m_nTop = y;
	return TRUE;
}

// 获得地图左上角点
BOOL CGameMap::GetLeftTop( int* px, int* py )
{
	*px = m_nLeft;
	*py = m_nTop;
	return TRUE;
}

// 移动地图
BOOL CGameMap::MoveMap( int nOffset_x, int nOffset_y )
{
	m_nLeft += nOffset_x;
	m_nTop  += nOffset_y;
	return TRUE;
}

// 敌人运动
BOOL CGameMap::MoveObjects( long lNow )
{
	// 减少事件延时计数器
	if( m_nEventDelay > 0 )
		m_nEventDelay --;

	if( m_pHero != NULL )
		m_pHero->Move( lNow );

	int i;
	for( i=0; i<m_nNpcCount; i++ )
		m_paNpcArray[i]->Move( lNow );

	if( m_pBoss != NULL )
		m_pBoss->Move( lNow );

	return TRUE;
}

// 绘制地图
void CGameMap::Draw( HDC hDC )
{
	if( m_hImageDC == NULL )
		return;

	// 绘制地图底层
	int i; int j;
	int aox; int aoy;    // 输出位置基点
	int ax;  int ay;     // 输出位置
	int map_ox; int map_oy;    // 地图矩阵坐标基点
	int map_x;  int map_y;     // 地图矩阵坐标
	int sx;  int sy;     // 源位置
	int nTile;           // 瓷砖编号
	aox = - (m_nLeft % TILE_W);
	aoy = - (m_nTop  % TILE_H);
	map_ox = m_nLeft / TILE_W;
	map_oy = m_nTop  / TILE_H;
	ax = aox;
	ay = aoy;
	map_x = map_ox;
	map_y = map_oy;
	for( i=0; i<=(GAME_AREA_H / TILE_H); i++ )
	{
		for( j=0; j<=(GAME_AREA_W / TILE_W); j++ )
		{
			if( (map_x<GRIDE_W) && (map_x>=0) && (map_y<GRIDE_H) && (map_y>=0) )
			{


				nTile = map[ map_x ][ map_y ];
				if( (nTile < 801) && (nTile > 0) )
				{
					// nTile的范围从0到800
					nTile --;
					sx=( (nTile % 40) * 32 );
					sy=( (nTile / 40) * 32);
					::BitBlt( hDC,ax,ay,TILE_W,TILE_H,m_hImageDC,sx,sy,SRCCOPY);
				}
				else{
					::BitBlt( hDC,ax,ay,TILE_W,TILE_H,m_hImageDC,0,0,BLACKNESS);
				}


#ifdef DEBUG
				int nEvent = npc[ map_x ][ map_y ];
				if( (nEvent < 801) && (nEvent > 0) )
				{
					// nTile的范围从0到800
					nEvent --;
					sx=( (nEvent % 40) * 32 );
					sy=( (nEvent / 40) * 32);
					::BitBlt( hDC,ax,ay,TILE_W,TILE_H,m_hNpcNumberDC,sx,sy,SRCPAINT);
					ax += TILE_W;
					map_x += 1;
					continue;
				}
#endif

			}
			ax += TILE_W;
			map_x += 1;
		}
		ax = aox;
		ay += TILE_H;
		map_x = map_ox;
		map_y += 1;
	}

#if 0
	// 输出左上角点的坐标
	::BitBlt( hDC, 0,0,100,50,hDC,0,0,WHITENESS );
	char psTemp[256];
	memset( psTemp, 0x0, 256 );
	sprintf( psTemp, "LEFT = %d", m_nLeft );
	::TextOut( hDC, 0, 0, psTemp, strlen(psTemp) );
	memset( psTemp, 0x0, 256 );
	sprintf( psTemp, "TOP  = %d", m_nTop );
	::TextOut( hDC, 0, 16, psTemp, strlen(psTemp) );
	if( m_pHero != NULL )
	{
		memset( psTemp, 0x0, 256 );
		sprintf( psTemp, "ACT  = %d", m_pHero->m_nStatus );
		::TextOut( hDC, 0, 32, psTemp, strlen(psTemp) );
	}
#endif

	// 按照Y序绘制GameObjects
	// 同一屏幕不会有超过100个的NPC
	CGameObject* pObjects[ 100 ];
	memset( pObjects, NULL, sizeof(CGameObject *)*100 );

	// 排序
	i = 0;
	int y;
	int nButtom = m_nTop + GAME_AREA_H + TILE_H*4;
	// 下面y扫描的范围比屏幕大一些，是考虑到如“棘球”这样的大物体
	for( y = m_nTop; y < nButtom; y ++ )
	{
		// 角色先绘制，同一个位置总是NPC遮挡角色
		if( m_pHero != NULL )
		{
			if( (m_pHero->m_y + m_pHero->m_h) == y )
			{
				pObjects[i] = m_pHero;
				if( ++i == 100 )
					goto paint;
			}
		}

		// NPC的绘制
		if( m_paNpcArray != NULL )
		{
			for( j=0; j<m_nNpcCount; j++ )
			{
				CNpc* pNpc = m_paNpcArray[j];

				if( pNpc->CanBeShown() )
				{
					if( (pNpc->m_y + pNpc->m_h ) == y )
					{
						pObjects[i] = m_paNpcArray[j];
						if( ++i == 100 )
							goto paint;
					}
				}
			}
		}

		// BOSS的绘制
		// 不管BOSS能不能看见，都要绘制BOSS的血条。
		if( m_pBoss != NULL )
		{
			if( ((m_pBoss->m_y + m_pBoss->m_h) == y) || (y == nButtom-1) )
			{
				pObjects[i] = m_pBoss;
				if( ++i == 100 )
					goto paint;
			}
		}
	}

paint:
	// 绘制
	for( i=0; i<100; i++ )
	{
		if( pObjects[i] != NULL )
			pObjects[i]->Draw( hDC );
		else
			break;
	}
}

/* 主角运动 */
BOOL CGameMap::GoUp( long lNow )
{
	return m_pHero->SetAct( HERO_ACT_GO_UP, lNow );
}
BOOL CGameMap::GoDown( long lNow )
{
	return m_pHero->SetAct( HERO_ACT_GO_DOWN, lNow );
}
BOOL CGameMap::GoLeft( long lNow )
{
	return m_pHero->SetAct( HERO_ACT_GO_LEFT, lNow );
}
BOOL CGameMap::GoRight( long lNow )
{
	return m_pHero->SetAct( HERO_ACT_GO_RIGHT, lNow );
}
BOOL CGameMap::Fire( long lNow )
{
	return m_pHero->SetAct( HERO_ACT_FIRE, lNow );
}
BOOL CGameMap::RenewCassette( long lNow )
{
	return m_pHero->SetAct( HERO_ACT_RENEW_CASSETTE, lNow );
}
// 主角运动部分结束

// 子弹飞行的碰撞检测处理
BOOL CGameMap::BulletGo( int bullet_power, int* bullet_x, int* bullet_y )
{
	//攻击原则：
	//1、找最近的打；
	//2、不打身后的；
	//3、不打地图以外的；
	//4、不打棘球


	int nStatus = m_pHero->m_nStatus % 20;

	int x, y, w, h;
	m_pHero->GetHitTestRect( &x, &y, &w, &h );

	// 子弹的碰撞检测面积是9个格
	// 这样做是为了使角色可以近距离防御，而且不用瞄得太准。
	if( (nStatus > 0) && (nStatus < 4) )
	{
		// 向上发射子弹
		x -= TILE_W;
		y -= 8;
		w  = TILE_W * 3;
		h  = 9;
	}
	else if( (nStatus > 3) && (nStatus < 7) )
	{
		// 向下发射子弹
		x -= TILE_W;
		y += TILE_H;
		w  = TILE_W * 3;
		h  = 9;
	}
	else if( (nStatus > 6) && (nStatus < 10) )
	{
		// 向左发射子弹
		x -= 8;
		y -= TILE_H;
		w  = 9;
		h  = TILE_H * 3;
	}
	else if( (nStatus > 9) && (nStatus < 13) )
	{
		// 向右发射子弹
		x += TILE_W;
		y -= TILE_H;
		w  = 9;
		h  = TILE_H * 3;
	}

	int map_x = m_nLeft + 8;
	int map_y = m_nTop  + 8;
	int map_w = GAME_AREA_W - 16;
	int map_h = GAME_AREA_H - 16;

	while( IsAreaCut( x,y,w,h, map_x, map_y, map_w, map_h ) )
	{
		//由近到远推移碰撞检测区域
		if( (nStatus > 0) && (nStatus < 4) ) {
			// 向上发射子弹
			y -= 8;
			x -= 8;
			w += 16;
		}
		else if( (nStatus > 3) && (nStatus < 7) ) {
			// 向下发射子弹
			y += 8;
			x -= 8;
			w += 16;
		}
		else if( (nStatus > 6) && (nStatus < 10) ) {
			// 向左发射子弹
			x -= 8;
			y -= 8;
			h += 16;
		}
		else if( (nStatus > 9) && (nStatus < 13) ) {
			// 向右发射子弹
			x += 8;
			y -= 8;
			h += 16;
		}

		// 检测与NPC的碰撞情况
		int i;
		for( i=0; i<m_nNpcCount; i++ )
		{
			int x2, y2, w2, h2;

			// 如果NPC是“棘球”也跳过
			if (m_paNpcArray[i]->IsSpikyBall())
				continue;

			// 获得NPC的碰撞检测矩形，如果NPC死亡则跳过
			if( !m_paNpcArray[i]->GetHitTestRect( &x2, &y2, &w2, &h2 ) )
				continue;

			if( IsAreaCut( x,y,w,h, x2,y2,w2,h2 ) )
			{
				// 攻击目标
				m_paNpcArray[i]->BeHit( bullet_power );

				// 着弹点位置是目标的中心
				int center_x = x2 + w2 / 2;
				int center_y = y2 + h2 / 2;

				*bullet_x = center_x - m_nLeft;
				*bullet_y = center_y - m_nTop;
				return TRUE;
			}
		}

		// 检测与BOSS的碰撞情况
		if( m_pBoss != NULL )
		{
			int x2, y2, w2, h2;

			// 获得NPC的碰撞检测矩形
			if( !m_pBoss->GetHitTestRect( &x2, &y2, &w2, &h2 ) )
				continue;

			if( IsAreaCut( x,y,w,h, x2,y2,w2,h2 ) )
			{
				// 攻击目标
				m_pBoss->BeHit( bullet_power );

				// 着弹点位置是目标的中心
				int center_x = x2 + w2 / 2;
				int center_y = y2 + h2 / 2;

				*bullet_x = center_x - m_nLeft;
				*bullet_y = center_y - m_nTop;
				return TRUE;
			}
		}
	}

	return FALSE;
}

// 攻击角色
void CGameMap::HitHero( int nPower )
{
	if( m_pHero == NULL )
		return;

	if( m_pHero->m_nLife == 0 )
		return;

	// 播放声音
	m_pMainFrm->m_pMidi->PlayWave( FILENAME_WAV_ENEMY_SHOOT );

	// nPower不超过400
	int nLevel = m_pHero->m_nLevel;
	int nInjure = nPower * 50 / nLevel;
	if( nInjure < 1 )
		nInjure = 1;  // 等级再高也要损失1点生命
	m_pHero->m_nLife -= nInjure;

	if( m_pHero->m_nLife <= 0 )
	{
		m_pHero->m_nLife = 0;
		m_pHero->m_wAct = HERO_ACT_DEAD;
		m_pHero->m_nStatus = 100;
		m_pMainFrm->m_pMidi->PlayWave( FILENAME_WAV_DEAD );
	}
}

// 增加角色的经验值
void CGameMap::AddExp( int nExp )
{
	if( m_pHero == NULL )
		return;

	// 播放NPC死亡的声音
	m_pMainFrm->m_pMidi->PlayWave( FILENAME_WAV_ENEMY_DEAD );

	// nExp不超过500
	m_pHero->m_nExp += nExp / 10;
	while( m_pHero->m_nExp > m_pHero->m_nLevel )
	{
		m_pHero->m_nExp -= m_pHero->m_nLevel;
		m_pHero->m_nLevel += 50;
		m_pHero->m_nLife = 1000;
		if( m_pHero->m_nLevel > 1000 )
			m_pHero->m_nLevel = 1000;
		m_pMainFrm->LevelUp();
	}
}

// 获得角色的显示属性（折合成百分比）
int CGameMap::GetHeroLife()
{
	if( m_pHero != NULL )
		return ( m_pHero->m_nLife / 10 );
	else
		return 0;
}

int CGameMap::GetHeroLevel()
{
	if( m_pHero != NULL )
		return ( m_pHero->m_nLevel / 10 );
	else
		return 0;
}

int CGameMap::GetHeroExp()
{
	if( m_pHero != NULL )
		return ( m_pHero->m_nExp / 10 );
	else
		return 0;
}

// 添加新的NPC
void CGameMap::AddNpc( int x, int y, int type )
{
	// 数值合法性判断
	if( x<0 || x>199 || y<0 || y>199 || type<1 || type>10 )
		return;

	// 清点NPC数组中有效NPC的数量
	int i;
	int nCount = 0;
	for( i=0; i<m_nNpcCount; i++ )
	{
		if( m_paNpcArray[i]->m_nType > 0 )
			nCount ++;
	}

	// 创建一个比有效NPC数量大1的数组
	CNpc** paNewNpcArray = new CNpc*[ nCount + 1 ];

	// 新NPC数组的第一个单元用于放置新加的NPC
	paNewNpcArray[0] = new CNpc( this, type, x*TILE_W, y*TILE_H );

	// 将老NPC数组中有效的NPC单元搬到新数组中
	// 老NPC数组中无效的NPC单元将被删除
	int j = 1;
	for( i=0; i<m_nNpcCount; i++ )
	{
		if( m_paNpcArray[i]->m_nType == 0 )
			delete m_paNpcArray[i];
		else
		{
			paNewNpcArray[j] = m_paNpcArray[i];
			j ++;
		}
	}

	// 释放老的NPC阵列，并用新的取而代之
	delete [] m_paNpcArray;
	m_paNpcArray = paNewNpcArray;
	m_nNpcCount = nCount + 1;
}

// 清除所有的NPC
void CGameMap::CleanNpc()
{
	// 释放NPC阵列
	int i;
	for( i=0; i<m_nNpcCount; i++ )
	{
		if( m_paNpcArray[i] != NULL )
			delete m_paNpcArray[i];
	}
	delete [] m_paNpcArray;
	m_nNpcCount = 0;
	m_paNpcArray = NULL;
}

// 设置BOSS
void CGameMap::SetBoss( int X, int Y, int event )
{
	if( m_pBoss != NULL )
		delete m_pBoss;

	int x = X * TILE_W;
	int y = Y * TILE_H;

	m_pBoss = new CBoss( this, x, y, event );
}

// 删除BOSS
void CGameMap::CleanBoss()
{
	if( m_pBoss != NULL )
	{
		delete m_pBoss;
		m_pBoss = NULL;
	}
}

// END