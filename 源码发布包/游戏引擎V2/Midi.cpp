// Midi.cpp

#include "stdafx.h"
#include "Game.h"
#include <process.h>   // 多线程支持


static void Thread( PVOID pvoid )
{
	CMidi* pMidi = (CMidi *)pvoid;

	while( pMidi->m_bRun )
	{
		WaitForSingleObject( pMidi->m_hEvent, INFINITE );

		switch( pMidi->m_nEvent )
		{
		case MIDI_EVENT_PLAY:
			pMidi->DoPlayMidi();
			break;
		case MIDI_EVENT_REPLAY:
			pMidi->DoReplayMidi();
			break;
		case MIDI_EVENT_STOP:
			pMidi->DoStopMidi();
			break;
		case MIDI_EVENT_QUIT:
			pMidi->m_nEvent = 0;
			_endthread();
			break;
		}
		pMidi->m_nEvent = 0;
	}

	pMidi->m_nEvent = 0;
	_endthread();
}

CMidi::CMidi()
{
	m_hWnd = NULL;
	m_wDeviceID = 0x0;//MCI装置代号
	m_dwReturn = 0x0;
	m_psMidiFile = new char[256];
	
	m_bRun = TRUE;
	m_hEvent = CreateEvent( NULL, FALSE, TRUE, NULL );
	m_nEvent = 0;

	_beginthread( Thread, 0, this );
}

CMidi::~CMidi()
{
	m_bRun = FALSE;
	m_nEvent = MIDI_EVENT_QUIT;
	SetEvent( m_hEvent );

	// 等待线程退出
	int i = 0;
	while( m_nEvent != 0 )
	{
		i ++;
		Sleep( 10 );
		if( i == 3 )
			break;
	}

	CloseHandle( m_hEvent );

	delete [] m_psMidiFile;
}

void CMidi::Init(HWND hWnd)
{
	m_hWnd = hWnd;
	m_mciOpenParms.lpstrDeviceType = "sequencer";
}

// 由于打开MCI设备需要一定的时间，所以开一个线程去做这个工作
void CMidi::PlayMidi(char* MidiFile)
{
    strncpy( m_psMidiFile, MidiFile, 255 );

	m_nEvent = MIDI_EVENT_PLAY;
	SetEvent( m_hEvent );
}

void CMidi::ReplayMidi(WPARAM w)
{
	if( m_wDeviceID!=0 && w==MCI_NOTIFY_SUCCESSFUL )
	{
		m_nEvent = MIDI_EVENT_REPLAY;
		SetEvent( m_hEvent );
	}
}

void CMidi::StopMidi()
{
	m_nEvent = MIDI_EVENT_STOP;
	SetEvent( m_hEvent );
}

//
DWORD CMidi::DoPlayMidi()
{
	DoStopMidi();

	char temp[256];

	m_mciOpenParms.lpstrElementName = m_psMidiFile; //要播放的MIDI
	m_mciOpenParms.wDeviceID = 0;

	// 打开
	m_dwReturn = mciSendCommand( 0, MCI_OPEN, MCI_OPEN_ELEMENT | MCI_OPEN_TYPE, (DWORD)(LPVOID)&m_mciOpenParms );
	if( m_dwReturn != 0 )
	{
		mciGetErrorString( m_dwReturn, temp, 255 );
		OutputDebugString( "DoPlayMidi, MCI_OPEN: " );
		OutputDebugString( temp );
	}

	m_wDeviceID = m_mciOpenParms.wDeviceID;

	//播放
    m_mciPlayParms.dwCallback = (DWORD)m_hWnd;
	m_dwReturn = mciSendCommand(m_wDeviceID, MCI_PLAY, MCI_NOTIFY, (DWORD)(LPVOID)&m_mciPlayParms);

	if( m_dwReturn != 0 )
    {
		mciGetErrorString( m_dwReturn, temp, 255 );
		OutputDebugString( "DoPlayMidi, MCI_PLAY: " );
		OutputDebugString( temp );
        mciSendCommand( m_wDeviceID, MCI_CLOSE, 0, NULL );
        return( m_dwReturn );
    }

	//
    return( 0L );
};

//
void CMidi::DoReplayMidi()
{
	char temp[256];

	m_dwReturn = mciSendCommand( m_wDeviceID, MCI_SEEK, MCI_SEEK_TO_START, NULL);
	if( m_dwReturn != 0 )
	{
		mciGetErrorString( m_dwReturn, temp, 255 );
		OutputDebugString( "DoReplayMidi, MCI_SEEK: " );
		OutputDebugString( temp );
	}

	m_dwReturn = mciSendCommand( m_wDeviceID, MCI_PLAY, MCI_NOTIFY, (DWORD)(LPVOID)&m_mciPlayParms);
	if( m_dwReturn != 0 )
	{
		mciGetErrorString( m_dwReturn, temp, 255 );
		OutputDebugString( "DoReplayMidi, MCI_PLAY: " );
		OutputDebugString( temp );
	}
}

//
void CMidi::DoStopMidi()
{
	char temp[256];

	if( m_wDeviceID != 0 )
	{
		m_dwReturn = mciSendCommand( m_wDeviceID, MCI_STOP, MCI_WAIT, NULL );
		if( m_dwReturn != 0 )
		{
			mciGetErrorString( m_dwReturn, temp, 255 );
			OutputDebugString( "DoStopMidi, MCI_STOP: " );
			OutputDebugString( temp );
		}

		m_dwReturn = mciSendCommand( m_wDeviceID, MCI_CLOSE, NULL, NULL );
		if( m_dwReturn != 0 )
		{
			mciGetErrorString( m_dwReturn, temp, 255 );
			OutputDebugString( "DoStopMidi, MCI_CLOSE: " );
			OutputDebugString( temp );
		}

		m_wDeviceID = 0;
	}
}

//
void CMidi::PlayWave(char* FileName)
{
	sndPlaySound( FileName, SND_ASYNC|SND_NODEFAULT );
}

//the end.
