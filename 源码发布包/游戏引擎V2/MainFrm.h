// MainFrm.h : CMainFrame 类的接口
//

#pragma once

// Win98下AlphaBlent函数的效率令人难以忍受，
// 屏蔽该宏定义，可以关闭程序中所有的Alpha混合
#define ALPHA_SUPPORT


// 定义四个按键掩码，用于判断哪个键是最后按下去的
#define KEY_STATUS_UP           0x01
#define KEY_STATUS_DOWN         0x02
#define KEY_STATUS_LEFT         0x04
#define KEY_STATUS_RIGHT        0x08
// 定义四个方向
#define DIRECTION_UP            1
#define DIRECTION_DOWN          2
#define DIRECTION_LEFT          3
#define DIRECTION_RIGHT         4

// 定义与脚本执行有关的常量
#define COMMAND_DIALOG             1
#define COMMAND_DELAY              2
#define COMMAND_DELAY_DIALOG       10

//控制三种枪械的射速
#define FIRE_RATE_GUN1         2  //MP5
#define FIRE_RATE_GUN2         3  //AK47
#define FIRE_RATE_GUN3         2  //PMK

// 子弹动画在子弹着弹点上的位置偏移
const int BulletOffset_x[4] = {  6, -8, 2, -4 };
const int BulletOffset_y[4] = { -7,  3, 6, -2 };

// 定义武器结构
typedef struct _WEAPEN
{
	BOOL bValid;                          // 是否拥有此枪
	int  nPower;                          // 一颗子弹的威力
	int  nCurAmmo;                        // 当前弹夹中子弹的数量
	int  nMaxAmmo;                        // 弹夹的容量
	int  nCount;                          // 子弹的总数(-1表示无限多)
	int  nRateCounter;                    // 控制射速的计数器
}WEAPEN;

// 定义存盘文件结构
typedef struct _SAVED
{
	struct _WEAPEN stWeapen[3];           // 三个武器数组，用于保存武器状态
	int nCurWeapen;
	int nLife;
	int nLevel;
	int nExp;
	int nScript;
}SAVED;

class CMidi;
class CGameMap;
class CGameBar;
class CScript;
class CBmp;
class CAvi;

class CMainFrame : public CFrameWnd
{
	
public:
	CMainFrame();
protected: 
	DECLARE_DYNAMIC(CMainFrame)

// 属性
public:
	// 商标
	CDC* m_pBrandDC;
	// 主缓冲区
	CDC* m_pBufDC;
	// 用于存放开始画面
	CDC* m_pTitleDC;
	// 用于存放状态栏背景
	CDC* m_pBarDC;
	// 用于存放各种按钮的位图
	CDC* m_pButtonsDC;
	// 用于临时存放对话框区域
	CDC* m_pDlgDC;
	// 用于背景音乐和声音播放的库
	CMidi* m_pMidi;
	// 游戏地图
	CGameMap* m_pGameMap;

	// 背景图片
	CBmp*    m_pBmp;
	// 动画数组
	CAvi*    m_pAvi;

	// 屏幕处于关闭状态（除对话栏外，什么都不显示）
	BOOL m_bScreenClosed;

	// 游戏状态
	int m_nST;
	// 1:开始界面；2:对话态；3:行走态；4:战斗态
	// 对话栏打开状态该值加10。

	// 当前鼠标所在的按钮
	int m_nCurButton;
	// 0:无；1:新的游戏；2:读取进度；3:退出游戏

	// 与升级动画有关的计数器
	int m_nLevelUpCounter;

	// 按键状态，用于判断上下左右四个键中哪个是最后按下去的
	BYTE m_byKeyStatus;
	// 上次行进的方向
	BYTE m_byDirection;

	// 上一条命令的类型
	int m_nLastCommandType;
	// 事件延时，防止事件被连续响应
	int m_nMessageDelay;

/* 与武器有关的变量及函数 */
private:

	// 当前武器
	int m_nCurWeapen;
	// 1:冲锋枪；2:自动步枪；3:轻机枪

	// 用于处理不能连续切换武器的状态字
	BOOL m_bTabKeyState;

	// 当前的武器状态
	struct _WEAPEN m_stGun1;
	struct _WEAPEN m_stGun2;
	struct _WEAPEN m_stGun3;

	// 与武器有关的绘制函数 
	void PaintGun1( CDC* pdc, BOOL bCur );
	void PaintGun2( CDC* pdc, BOOL bCur );
	void PaintGun3( CDC* pdc, BOOL bCur );

	// 用于存放子弹爆炸的动画特效
	int m_paBullet[4][3];

/* 与武器有关的变量及函数定义结束 */

/* 与对话栏有关的变量 */
	// 与“Next”按钮绘制有关的变量
	CDC* m_pRainbowDC;       // 彩虹位图
	int m_nChangeColor;      // 控制Next按钮边框颜色变化
	int m_nButtonNextColor;  // Next按钮的边框颜色
	CDC* m_pLeftPhotoDC;     // 左边的头像
	CDC* m_pRightPhotoDC;    // 右边的头像
	CDC* m_pItemPhotoDC;     // 中间的物品图片
	char m_str1[64];         // 第一行显示的文字
	char m_str2[64];         // 第二行显示的文字
	char m_str3[64];         // 第三行显示的文字
	char m_str4[64];         // 第四行显示的文字
/* 与对话栏有关的变量定义结束 */

	// 脚本解释机
	CScript* m_pScript;

// 操作
public:
	// 显示开始画面
	void ShowStart();

	// 在Buffer中准备好需要绘制的内容
	void MakeBuffer();

	// 绘制
	void Paint();

	// 动作推进（得到CPU时间片）
	void ActProc( long lNow );

/* 角色动作 */
	// 切换武器
	void OnChangeWeapon( long lNow );
	// 射击
	void OnFire( long lNow );
	// 重装弹夹
	void OnRenewCassette( long lNow );
/* 角色动作结束 */

	// 绘制状态栏
	void DrawBar( CDC* pdc );

	// 绘制子弹击中物体的效果
	void DrawBullets( CDC* pdc );

	// 升级（生命条和经验条背景变成金色，并显示“Level UP”）
	void LevelUp();

	// 角色死亡
	void HeroDead();

	// 屏幕从黑色背景中淡出
	void OpenScreen();
	// 屏幕暗下变成黑色背景
	void CloseScreen();
	// 震动画面
	void ShakeScreen();

	// 绘制对话栏
	void DrawDialog( CDC* pdc );

	// 显示对话栏
	void ShowDialog( int lface, int rface, int item, int filename, int line );
	// lface:左边的头像；rfact:右边的头像；item:中间的物品；
	// filename:剧本文件；line:起始行号。

	// 关闭对话栏
	void CloseDialog();

	// 根据ID号打开bmp文件创建HDC
	BOOL OpenBmpByID( CDC* pPhotoDC, int id );

	// 根据ID号和行号打开dlg文件，读出4行对话内容
	BOOL OpenDlgByID( int file, int line );

	// 开始新的游戏
	void InitGame();
	// 读取进度
	BOOL LoadGame();
	// 保存进度
	BOOL SaveGame( int nScript );

	// 脚本处理
	BOOL ScriptProc();

	// 释放所有的资源
	void ReleaseAll();

	// 退出游戏的处理
	void QuitGame();

/* 命令解释执行函数 */
	// 循环播放Midi
	BOOL CmdPlayMidi( CString& strCommand );
	// 播放音效
	BOOL CmdPlayWav( CString& strCommand );
	// 显示图片
	BOOL CmdShowBmp( CString& strCommand );
	// 显示动画序列
	BOOL CmdShowAvi( CString& strCommand );
	// 隐藏动画序列
	BOOL CmdHideAvi( CString& strCommand );
	// 显示对话栏
	BOOL CmdDialog( CString& strCommand );
	// 延时
	BOOL CmdDelay( CString& strCommand );
	// 设置武器
	BOOL CmdSetGun( CString& strCommand );
	// 添加武器
	BOOL CmdAddGun( CString& strCommand );
	// 隐藏武器
	BOOL CmdDeleteGun( CString& strCommand );
	// 添加子弹
	BOOL CmdAddAmmo( CString& strCommand );
	// 设置角色图片
	BOOL CmdSetHeroImage( CString& strCommand );
	// 设置副角色图片
	BOOL CmdSetHeroImage2( CString& strCommand );
	// 设置角色朝向
	BOOL CmdSetHeroDirection( CString& strCommand );
	// 角色提升一级
	BOOL CmdLevelUp();
	// 打开地图文件
	BOOL CmdOpenMap( CString& strCommand );
	// 设置地图显示的左上角位置
	BOOL CmdSetMapLeftTop( CString& strCommand );
	// 打开脚本文件
	BOOL CmdOpenScript( CString& strCommand );
	// 设置游戏状态
	BOOL CmdSetGameStatus( CString& strCommand );
	// 设置地图值
	BOOL CmdSetMap( CString& strCommand );
	// 设置NPC值
	BOOL CmdSetNpc( CString& strCommand );
	// 添加NPC
	BOOL CmdAddNpc( CString& strCommand );
	// 设置BOSS位置，开始BOSS作战
	BOOL CmdSetBoss(  CString& strCommand );
	// 保存游戏进度
	BOOL CmdSaveGame( CString& strCommand );
/* 命令解释执行函数结束 */

// 重写
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

// 实现
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// 生成的消息映射函数
protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	DECLARE_MESSAGE_MAP()
protected:
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
public:
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	LRESULT OnGameMessage( WPARAM type, LPARAM lParam );
};


